/*
 * Morsegeneraator.c
 *
 * Created: 14-May-18 9:53:57 PM
 * Author : Robert
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>

#define BAUD_RATE 9600L
#define F_CPU 2000000L
#define UBRR_VAL ((unsigned int)(F_CPU/(16*BAUD_RATE))-1)
#define bufferSize 24
const uint32_t morseBinary[] = { //ASCII table order, reversed binary representation, 0b1 is end sign
	0b10000, // space, length 4, because each char has a 3 unit pause after
	0b11110111010111010111, // !
	0b1101110101011101, // "
	0b1, // #
	0b111101010111010101, // $
	0b1, // %
	0b110101011101, // &
	0b11011101110111011101, // '
	0b1101110111010111, // (
	0b11110101110111010111, // )
	0b1, // *
	0b11011101011101, // +
	0b11110111010101110111, // ,
	0b1111010101010111, // -
	0b111101011101011101, // .
	0b11011101010111, // /
	0b11110111011101110111, //0
	0b111101110111011101, //1
	0b1111011101110101, //2
	0b11110111010101, //3
	0b111101010101, //4
	0b1101010101, //5
	0b110101010111, //6
	0b11010101110111, //7
	0b1101011101110111, //8
	0b110111011101110111, //9
	0b110101011101110111, //:
	0b110111010111010111, //;
	0b1, //<
	0b11110101010111, //=
	0b1, //>
	0b1101011101110101, //?
	0b110111010111011101, //@
	0b111101,   //A
	0b1101010111, //B
	0b110111010111, //C
	0b11010111,  //D
	0b11,    //E
	0b1101110101, //F
	0b1101110111,  //G
	0b11010101, //H
	0b1101,   //I
	0b11110111011101, //J
	0b1111010111,  //K
	0b1101011101, //L
	0b11110111,   //M
	0b110111,   //N
	0b111101110111,  //O
	0b110111011101, //P
	0b11110101110111, //Q
	0b11011101,  //R
	0b110101,  //S
	0b1111,    //T
	0b11110101,  //U
	0b1111010101, //V
	0b1111011101,  //W
	0b111101010111, //X
	0b11110111010111, //Y
	0b110101110111 //Z
};

volatile uint32_t playableChar = 2;
volatile uint32_t uartBuffer[bufferSize]; // Ringbuffer
volatile uint8_t inIndex = 0;
volatile uint8_t outIndex = 0;
volatile uint8_t pause = 3;
void putCharacter(uint8_t inByte);
void buzzerON();
void buzzerOFF();
void switchBuzzer();
void getADC(uint16_t value);
void disableJTAG();
void enableBuzzer();
void enableBuzzerPWM();
void initADC();
void initPulseCounter();
void initUART();


ISR(ADC_vect){
	uint16_t ADCvalue = ADC;
	getADC(ADCvalue);
}

ISR(TIMER3_COMPA_vect){
	if (playableChar == 0b1){
		buzzerOFF();
		PORTA |= 8;
		if ((pause == 0)){ //Time between letters
			pause = 3;
			if (inIndex != outIndex){
				playableChar = uartBuffer[outIndex];
				if (outIndex == bufferSize){
					outIndex = 0;
				}
				else{
					outIndex++;
				}
			}
		} 
		else{
			pause -= 1;
		}
	}
	else{
		PORTA &= !8;
		uint8_t sound = playableChar & 1;
		if (sound == 1){
			buzzerON();
		}
		else{
			buzzerOFF();
		}
		playableChar = playableChar >> 1;
	}
	ADCSRA |= (1 << ADSC);
	PORTA ^= 1;
}

ISR(USART1_RX_vect){
	uint8_t inByte = UDR1;
	putCharacter(inByte);
}

int main(void){
	disableJTAG();
    enableBuzzer();
	initADC();
	initPulseCounter();
	initUART();

	sei();
	ADCSRA |= (1 << ADSC); //Start ADC measurement
    while (1){
    }
}

void putCharacter(uint8_t inByte){
	if ((inIndex == outIndex - 1) | ((inIndex == 0) & (outIndex == 20))){//Does not overwrite
		return;
	}
	int8_t index = inByte - 32;
	if ((97 - ' ' <= index) & (index <= 122 - ' ')){ //To upper case
		index -= 32;
	}
	if ((0 > index) | (index > 58)){ //Out of range
		uartBuffer[inIndex] = 0b10;
	}
	else{
		uartBuffer[inIndex] = morseBinary[index];
	}
	if (inIndex == bufferSize){
		inIndex = 0;
	}
	else{
		inIndex++;
	}
}

void buzzerON(){
	DDRB |= (1 << PINB6);
}

void buzzerOFF(){
	DDRB &= !(1 << PINB6);
}
void switchBuzzer(){
	DDRB ^= (1 << PINB6);
}

void getADC(uint16_t value){
	//sets min value
	if(value < 150){
		value = 150;
	}
	OCR3A = value;
}

void disableJTAG(){
	uint8_t j = MCUCR | (1 << JTD);
	MCUCR = j;
	MCUCR = j;
}

void enableBuzzer(){
	//pin b6 as output
	DDRB = (1 << PINB6);
	//LED indicator
	DDRA = 0xFF;
	enableBuzzerPWM();
	buzzerOFF();
}

void enableBuzzerPWM(){
	//tc1 for buzzer pwm
	TCCR1A = (1 << COM1B1) | (1 << WGM10); //clear buzzer pin on comp. match - set at TOP, 8-bit fast pwm
	TCCR1B = (1 << WGM12) | (1 << CS11); //clk/8 prescaler
	OCR1B = 30000; //45% duty cycle
}

void initADC(){
	ADMUX = (1 << REFS0) | (1 << MUX1); //
	ADCSRA = (1 << ADEN) | (1 << ADSC) | (1 << ADIE) | (1 << ADPS2);
}

void initPulseCounter(){
	TCCR3B = (1 << WGM32) | (1 << CS32) | (1 << CS30);
	TIMSK3 = (1 << OCIE3A);
	OCR3A = 100;
}

void initUART(){
	UBRR1 = UBRR_VAL; //9600 baud
	UCSR1B = (1 << RXCIE1) | (1 << RXEN1);
	UCSR1C = (1 << UCSZ11) | (1 << UCSZ10);
}